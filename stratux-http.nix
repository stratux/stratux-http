{ mkDerivation, aeson, base, HTTP, network-uri, stdenv
, stratux-types, transformers, utf8-string
}:
mkDerivation {
  pname = "stratux-http";
  version = "0.0.12";
  src = ./.;
  libraryHaskellDepends = [
    aeson base HTTP network-uri stratux-types transformers utf8-string
  ];
  homepage = "https://gitlab.com/stratux/stratux-http";
  description = "A library for using HTTP with stratux";
  license = stdenv.lib.licenses.bsd3;
}
