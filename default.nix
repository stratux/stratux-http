{ nixpkgs ? import <nixpkgs> {}, compiler ? "default" }:

let

  inherit (nixpkgs) pkgs;

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  sources = {
    papa = pkgs.fetchFromGitHub {
      owner = "qfpl";
      repo = "papa";
      rev = "97ef00aa45c70213a4f0ce348a2208e3f482a7e3";
      sha256 = "0qm0ay49wc0frxs6ipc10xyjj654b0wgk0b1hzm79qdlfp2yq0n5";
    };

    stratux-types = pkgs.fetchFromGitLab {
      owner = "stratux";
      repo = "stratux-types";
      rev = "4c523092ceb0caceb4d45ef98c4a720ed9a1ac6a";
      sha256 = "16cv092fd4sq18gyqfqm5f334hb01knid3wy10xfypd39kix55m5";
    };

  };

  modifiedHaskellPackages = haskellPackages.override {
    overrides = self: super: import sources.papa self // {
      stratux-types = import sources.stratux-types {};
      parsers = pkgs.haskell.lib.dontCheck super.parsers;        
    };
  };

  stratux-http = modifiedHaskellPackages.callPackage ./stratux-http.nix {};

in

  stratux-http

